module.exports = {
  apps : [{
    name: "nodeapp",
    script: './bin/www',
    instances: 4,
    exec_mode: "cluster",
    watch  : false
  }],
  deploy : {
    production : {
      user : 'SSH_USERNAME',
      host : 'SSH_HOSTMACHINE',
      ref  : 'origin/master',
      repo : 'GIT_REPOSITORY',
      path : 'DESTINATION_PATH',
      'pre-deploy-local': '',
      
      'pre-setup': ''
    }
  }
};
