const Status = {
    BAD_REQUEST: 400,
    INTERNAL_SERVER_ERROR: 500,
    FORBIDDEN : 403,
    CONFLICT: 409,
    DEPENDENCY_ERROR : 424,
    UNAUTHORIZED_ERROR : 401
}

const ErrorConstants = {
    ERR_CONCURRENCY_FAILURE : "error.concurrencyFailure",
    ERR_VALIDATION: "error.validation",
    PROBLEM_BASE_URL : "baseURL",
    DEFAULT_TYPE :  "problem-with-message",
    CONSTRAINT_VIOLATION :  "constraint-violation",
    INVALID_PASSWORD_TYPE :  "invalid-password",
    EMAIL_ALREADY_USED_TYPE :  "email-already-used",
    LOGIN_ALREADY_USED_TYPE :  "login-already-used",
    EMAIL_NOT_FOUND_TYPE :  "email-not-found",
    INVALID_ACCESS_TYPE : "invalid-access",
    INVALID_DATE : "invalid-date",
    DEPENDENCY_ERROR_TYPE : "dependency-error",
    ACTIVATION_KEY_ERROR :  "activation-key-error",
    UNAUTHORIZED_ERROR : "unauthorized"
}

const helperURL = "https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/";

function InvalidPasswordException(res, err) {
    return res.status(Status.BAD_REQUEST).json({
        "success": false,
        "type": ErrorConstants.INVALID_PASSWORD_TYPE,
        "message":  "Incorrect password",
        "status": Status.BAD_REQUEST
    });
}

function EmailNotFound(res, err){
    return res.status(Status.BAD_REQUEST).json({
        "success": false,
        "type": ErrorConstants.EMAIL_NOT_FOUND_TYPE,
        "message": "Email address not registered",
        "status":  Status.BAD_REQUEST
    });
}

function EmailAlreadyUsed(res, err){
    return res.status(Status.CONFLICT).json({
        "success": false,
        "type": ErrorConstants.EMAIL_ALREADY_USED_TYPE,
        "message": "Email is already in use!",
        "status": Status.CONFLICT
    })
}
function InternalServerError(res, err){
   return res.status(Status.INTERNAL_SERVER_ERROR).json({
        "success": false,
        "message":  err.message ? err.message : "",
        "status": Status.INTERNAL_SERVER_ERROR,
        "type": ErrorConstants.DEFAULT_TYPE,
        "link": helperURL + Status.INTERNAL_SERVER_ERROR
    });
}
function ForbiddenRequest(res, err){
    res.status(Status.FORBIDDEN).json({ 
        "success": false,
        "message": err.message ? err.message : "",
        "developerMessage" : err.developerMessage ? err.developerMessage : "", 
        "status" : Status.FORBIDDEN,
        "type": ErrorConstants.INVALID_ACCESS_TYPE,
        "link": helperURL + Status.FORBIDDEN
    });
}
function InvalidDate(res, err){
    return res.status(Status.BAD_REQUEST).json({ 
        "success": false,
        "type": ErrorConstants.INVALID_DATE,
        "message": err.message ? err.message : "",
        "status" : Status.BAD_REQUEST
    });
}

function DependencyError(res, err){
    return res.status(Status.DEPENDENCY_ERROR).json({ 
        "success": false, 
        "type": ErrorConstants.DEPENDENCY_ERROR_TYPE,
        "message": err.message ? err.message : "",
        "developerMessage" : err.developerMessage ? err.developerMessage : "", 
        "status" : Status.DEPENDENCY_ERROR
    });
}
function ActivationKeyError (res, err) { 
    return res.status(Status.INTERNAL_SERVER_ERROR).json({
         "success": false,
         "type": ErrorConstants.ACTIVATION_KEY_ERROR,
         "message" : "No user was found for the activation key",
         "status" : Status.INTERNAL_SERVER_ERROR
    });
}

function UnauthorizedError(res, err){
    return res.status(Status.UNAUTHORIZED_ERROR).json({ 
        "success" : false, 
        "message" : 'Invalid Token',
        "status" : Status.UNAUTHORIZED_ERROR,
        "type": ErrorConstants.UNAUTHORIZED_ERROR,
        "link": helperURL + Status.UNAUTHORIZED_ERROR
    });
}

function ValidationError(res, err){
    return res.status(Status.BAD_REQUEST).json({ 
        "success": false,
        "type": ErrorConstants.ERR_VALIDATION,
        "message": err.message,
        "status": Status.BAD_REQUEST
    });
}

function InvalidRequestBody(res, err){
    return res.status(Status.BAD_REQUEST).json({
        "success": false,
        "type": ErrorConstants.ERR_VALIDATION,
        "message": 'Request body is Invalid.',
        "status": Status.BAD_REQUEST
    });
}

module.exports = {
    ValidationError,
    UnauthorizedError,
    EmailNotFound,
    InvalidDate,
    ActivationKeyError,
    InvalidRequestBody,
    ForbiddenRequest,
    DependencyError,
    InternalServerError
}