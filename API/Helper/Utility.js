module.exports = {
    isDateValid,
    generateTimestampQuery
};

function isDateValid(date, raiseError) {
    if(date.toString() === "Invalid Date"){
        if(raiseError){
            var e = new Error("");
            e.name = "Invalid Date";
            e.message = "Date is invalid";
            throw e;
        }
    }
}

function generateTimestampQuery(data){
    let object = {};
    let date = new Date();
    if(data.userId){
        object["userId"] = data.userId
    }
    if(data.date1){
        date = new Date(data.date1);
        isDateValid(date, true);
    }
    /*if(data.days != undefined){
        date = new Date(new Date().setDate(new Date().getDate() - data.days))
    }
    if(data.hours != undefined){
        date.setHours(date.getHours() - data.hours);
    }*/
    if(data.data != undefined && data.data === "pre"){ 
        object["timestamp"] = { $lt : date};
    }else if(data.data != undefined && data.data === "post"){ 
        object["timestamp"] = { $gt : date};   
    }else if(data.data != undefined && data.data === "between"){ 
        let date2 = new Date();
        if(data.date2){
            date2 = new Date(data.date2);
            isDateValid(date2, true);
        }
        object["timestamp"] = { $lt: date2,  $gt : date};   
    }
    //console.debug(object);
    return object;
}
