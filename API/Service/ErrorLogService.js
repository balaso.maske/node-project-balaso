const db = require("../Database/db");
const ErrorLog = db.ErrorLog;
const logger = require("../logger");
const util = require("../Helper/Utility")

module.exports = {
    addErrorLog,
    removeErrorLog,
    getErrorLogs,
    getErrorLogsByParam
};

async function addErrorLog(logParam) {
    try{
        const log = new ErrorLog(logParam);
        await log.save();
    }catch(err){
        console.log(err);
    }
}

async function removeErrorLog(){
    logger.info("  Remove Error Log service called days -->  "+ this.days);
    try{
        await ErrorLog.deleteMany({ "timestamp" : { $lt: new Date(new Date().setDate(new Date().getDate() - this.days))}});
    }catch(err){
        console.log(err);
    }
}
async function getErrorLogs(req, res){
    try {
        return await ErrorLog.find();
    }catch(err){
        console.log(err);
    }
}

async function getErrorLogsByParam(req, res, next){
    return await ErrorLog.find(util.generateTimestampQuery(req.body));
}





