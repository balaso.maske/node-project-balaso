const db = require("../Database/db");
const RequestLog = db.RequestLog;
const logger = require("../logger");


module.exports = {
    addRequestLog,
    removeRequestLog
};

function addRequestLog(logParam) {
    try{
        const log = new RequestLog(logParam);
        log.save();
    }catch(err){
        console.log(err);
    }
}

async function removeRequestLog(){
    logger.info("  Remove request Log service called days -->  "+ this.days);
    try{
        await RequestLog.deleteMany({ "timestamp" : { $lt: new Date(new Date().setDate(new Date().getDate() - this.days))}});
    }catch(err){
       console.log(err);
    }
}
