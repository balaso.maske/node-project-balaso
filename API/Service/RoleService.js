const db = require("../Database/db");
const Role = db.Role;
const User = db.User;
const paginate = require("../Helper/Pagination");

module.exports = {
    getAll,
    addRole,
    getUserRole,
    deleteRole,
    update
};

async function getAll(req, res) {
    //return await .find().select("-__v").populate("pages", "_id name url");
    var paginateParam = paginate.paginationParam(req.query);
    const result = Role.aggregate([
        { $match : {}},
        {"$sort" : paginateParam.sortCondition },
          {
            $facet: {
              data : [ { $lookup: { from: 'pages', localField: 'pages', foreignField: '_id', as: 'pages' }},
                { "$skip": paginateParam.skip },{ "$limit": paginateParam.pageSize }],
              total: [ { $count: 'total'} ]
            }
          }
        ]);
    return result;
}



async function addRole(req, res) {

    let roleParam = req.body;
    
    const role = new Role(roleParam);
    role.createdBy = req.userInfo.email;
    role.lastModifiedBy = req.userInfo.email;

    return await role.save().then(savedRole => {
        return savedRole;
    });
}

async function getUserRole(req, res){
    let uname = req.userInfo.username;
   const result = await User.aggregate([{
    $match:{
      username: uname 
    }
   },
   { $lookup:
      {
        from: 'roles',
        localField: 'roles',
        foreignField: '_id',
        as: 'roles'
      }
    },    
   {
    $project:{
       username:"$username",
       "roles._id" : 1,        
        "roles.isSysRole" :1 ,
        "roles.name" :1,
        "roles.pages" : 1
        
    }
 }
   ], function (err, result) {
            if (err) {
                return err;
            }
            
            return { success : true, data: result};
        });

    return result;
}

async function deleteRole(req, res , id) {
    const role = await Role.findById(id);

    if(!role){
        throw "Role not available for delete";
    }
    if(role.isSysRole){
        var e = new Error
        e.name = 'Forbidden';
        e.message = "You can't delete system role";
        throw e;
    }

    const result = await Role.aggregate([{
        $match:{
          "name" : role.name
        }
       },
       { $lookup:
          {
            from: 'users',
            localField: '_id',
            foreignField: 'roles',
            as: 'users'
          }
        },    
       {
        $project:{
           "name" : 1,
           "isSysRole" :1,
           "users.username" : 1,        
            "users.email" :1 
            
        }
    }
       ]);

       if(result[0].users.length == 0){
           await Role.findByIdAndRemove(id);
       }else{
        var e = new Error
        e.name = 'DependencyError';
        e.message = "Dependent User available please remove role from these users.";
        e.developerMessage = " roles : " + JSON.stringify(result[0].users);
        throw e;
       }
}

async function update(req, res, next) {
    
    const id = req.body._id;
    if(id === undefined){
        throw "Role id not Available ";
    }

    const roleParam = req.body;
    const role = await Role.findById(id);

    if (!role) { throw 'Role not found';}

    if (role.name !== roleParam.name && await Role.findOne({ name: roleParam.name })) {
        throw 'Role "' + roleParam.name + '" is already taken';
    }

    roleParam.lastModifiedDate = Date.now();
    roleParam.lastModifiedBy = req.userInfo.email

    // copy roleParam properties to role
    Object.assign(role, roleParam);
    return await role.save().then(updatedRole => {
        return updatedRole;
    });
}