const schedule = require('node-schedule');

module.exports = {
    everySecondScheduler,
    setScheduler,
    customScheduler
}
async function everySecondScheduler(callback){
    schedule.scheduleJob('1 * * * * *', function(){
        console.log(" Run every minute ");
        if (callback && (typeof callback == "function")) {
            console.log(callback);
            callback();   
         }
    });
}

async function setScheduler(){
    //{hour: 16, minute: 57, dayOfWeek: 3}
    schedule.scheduleJob({hour: 18, minute: 23}, function(){
        console.log(" Run at "+ Date.now());
    });
}

async function customScheduler(schedulerTime, paramFunction){
    //{hour: 18, minute: 23}
    //{hour: 16, minute: 57, dayOfWeek: 3}
    console.log(typeof schedulerTime);
    if(typeof schedulerTime === "object"){

    }

    schedule.scheduleJob(schedulerTime, function(){
        console.log(" Run time "+ schedulerTime);
        if (paramFunction && (typeof paramFunction == "function")) {
            paramFunction();   
        }
    });
}

