const logger = require("../logger");
const errorLogService = require("../Service/ErrorLogService");
const Exception = require("../Exception/Exception")

module.exports = errorHandler;

function errorHandler(err, request, res, next) {
    const { method, url, headers, body } = request;
    const clientIp = request.ip;
    let userId = "0";
    
    if(request.userInfo !== undefined){
        userId = request.userInfo._id;
    }

    let obj = {
        "method" : method,
        "url" : url,
        "IP" : clientIp,
        "userId" : userId,
        "request" : body,
        "status" : 0,
        "errMessage" : ""
    }

    if (typeof (err) === 'string') {
        // custom application error
        return res.status(400).json({ success: false, message: err });
    }
    switch(err.name){
        case 'ValidationError':
            obj["status"] = 400;
            obj["errorMessage"] = err.message;
            errorLogService.addErrorLog(obj);
            logger.log("error", obj);
            Exception.ValidationError(res, err);
            break;
        case "UnauthorizedError":
            Exception.UnauthorizedError(res, err);
            break;
        case "EmailNotFound":
            Exception.EmailNotFound(res, err);
            break;
        case "Invalid Date":
            Exception.InvalidDate(res, err);
            break;
        case "ActivationKey":
            Exception.ActivationKeyError(res, err);
            break;
        case "InvalidRequestBody":
            Exception.InvalidRequestBody(res, err);
            break;
        case "Forbidden":
            obj["status"] = 403;
            obj["errorMessage"] = err.message;
            errorLogService.addErrorLog(obj);
            logger.log("error", obj);
            Exception.ForbiddenRequest(res, err);
            break;
        case "DependencyError":
            obj["status"] = 500;
            obj["errorMessage"] = err.message;
            errorLogService.addErrorLog(obj);
            logger.log("error", obj);
            Exception.DependencyError(res, err);
            break;
        default:
            Exception.InternalServerError(res, err);
    }
}