const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');

const ErrorLogSchema = new Schema({
    timestamp: {
        type: Date,
        default: Date.now,
        index: true
    },
    method: {
        type: String,
        default: ""
    },
    url :{
        type: String,
        required: [true, 'URL is required'],
        index: true
    },
    IP: {
        type: String,
        default: ""
    },
    
    userId: {
        type: String,
        default: "0",
        index: true
    },
    request: {
        type: Object,
        default: {}
    },
    status: {
        type: Number,
        default: 0
    },
    errorMessage: {
        type: Object,
        default: {}
    }
});

ErrorLog = module.exports = mongoose.model('ErrorLog', ErrorLogSchema);

ErrorLog.ensureIndexes(function(err){
    if(err){
        console.log(err);
    }
});