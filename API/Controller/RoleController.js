const express = require('express');
const router = express.Router();
const roleService = require("../Service/RoleService");


const isAllowed = require("../Auth/permission");

router.get('/roles', isAllowed("Admin"), getAllRoles);
router.post("/roles", isAllowed("Admin"), addRole)
router.put('/roles', isAllowed("Admin"), updateRole);


router.get('/userRole', getUserRole);
router.delete('/roles/:id', deleteRole);

module.exports = router;

function getAllRoles(req, res, next) {
    roleService.getAll(req, res)
        .then((roles) => res.json( { success: true, data : roles }))
        .catch(err => next(err));
}

function addRole(req, res, next) {
    roleService.addRole(req, res)
        .then((roles) => res.json({ success: true, data : roles }))
        .catch(err => next(err));
}

function updateRole(req, res, next) {
    roleService.update(req, res)
        .then(() => res.status(200).json({success: true, message:"Role updated successfully"}))
        .catch(err => next(err));
}

function getUserRole(req, res, next) {
    roleService.getUserRole(req, res)
        .then((roles) => res.status(200).json({success: true, data : roles}))
        .catch(err => next(err));
}

function deleteRole(req, res, next) {
    roleService.deleteRole(req, res, req.params.id)
        .then(() => res.json({ "success": true, message : "Role Deleted Successfully"}))
        .catch(err => next(err));
}