const express = require('express');
const router = express.Router();
const errorLogService = require("../Service/ErrorLogService");


const isAllowed = require("../Auth/permission");

router.get('/errorlogs',  isAllowed("Admin"), getErrorLogs);
router.post('/errorlogs',  isAllowed("Admin"), getErrorLogsByParam);


module.exports = router;

function getErrorLogs(req, res, next) {
    errorLogService.getErrorLogs(req, res)
        .then((logs) => res.json( { success: true, data : logs }))
        .catch(err => next(err));
}

function getErrorLogsByParam(req, res, next) {
    errorLogService.getErrorLogsByParam(req, res, next)
        .then((logs) => res.json( { success: true, data : logs }))
        .catch(err => next(err));
}