var createError = require('http-errors');
const compression = require('compression')
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
const errorHandlers = require("./API/ErrorHandler/ErrorHandler");
var rfs = require('rotating-file-stream') // version 2.x
const logger = require("./API/logger");

var onFinished = require('on-finished');

const schedule = require("./API/Scheduler/scheduler");

const requestLogService = require("./API/Service/RequestLogService");

//schedule.customScheduler("1 * * * * *", requestLogService.removeRequestLog.bind({"days" :1}));

var fs = require('fs')
var path = require('path')

// create a write stream (in append mode)
//var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })

// create a rotating write stream
var accessLogStream = rfs.createStream('access.log', {
    interval: '2d', // rotate 2 day
    path: path.join(__dirname, 'log'),
})

const jwt = require("./API/Auth/jwt");

const app = express();
app.use(compression());

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.all('*', function(request, res, next){
    const start = Date.now();
    const { method, url, headers, body } = request;
 
    const clientIp = request.ip;

    onFinished(res, function (err, res) {
        const ms = Date.now() - start;
        let userId = "0";
        if(request.userInfo !== undefined){
            userId = request.userInfo._id;
        }
        let obj = {
            "method" : method,
            "url" : url,
            "IP" : clientIp,
            "userId" : userId,
            "startTime" : start,
            "endTime": Date.now(),
            "TotalTimeRequired" : ms
        }
        
        var responseObj = {
            "status" : res.statusCode
        };
        obj["response"] = responseObj;
        requestLogService.addRequestLog(obj);
        if(err){
            console.log(err);
        }
    })
    next();
});
app.use(morgan('combined', { stream: accessLogStream }));

// use JWT auth to secure the api
app.use(jwt());

app.use('/api', require("./API/Controller/AccountController"));
app.use('/api', require("./API/Controller/RoleController"));
app.use('/api', require("./API/Controller/UserController"));
app.use('/api', require("./API/Controller/PageController"));
app.use('/api', require("./API/Controller/ErrorLogController"));

app.use(errorHandlers);

app.get('/', function(request, res, next){
    res.send("<body> Application works<br/> <br/> <a href='/downloadCollection'> Click here to download JSON collection </a></body> ");
});

app.get('/downloadCollection', function(request, res, next){
    const file = "./API/Helper/SamplePostmanCollection.json";
    res.download(file); // Set disposition and send it.
});
// start server
const port = app.get("port") || 4444;

console.log(process.env.NODE_ENV);

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

const server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
    logger.log("info", "Server listening on port " + port);
});


module.exports = app;
